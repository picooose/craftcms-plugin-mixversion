Mix Version
===================

Return assets version from the Laravel Mix manifest

## Usage

``` twig
{{ mix('index.css') }}
{{ mix('dist/app.js') }}
```
