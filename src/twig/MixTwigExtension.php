<?php
/**
 * MixVersion plugin for Craft CMS
 *
 * Mix Twig Extension
 *
 * @author    Adrien Picard
 * @copyright Copyright (c) 2017 Adrien Picard
 * @package   Mix
 * @since     0.1.0
 */

namespace apstudio\mixversion\twig;

use Craft;
use Twig_Extension;
use apstudio\mixversion\services\MixService;

class MixTwigExtension extends \Twig_Extension
{
    /**
     * @return string The extension name
     */
    public function getName()
    {
        return 'Mix';
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('mix', [$this, 'readFromManifest']),
        ];
    }

    /**
     * readFromManifest method.
     *
     * @see MixService::get()
     *
     * @param string $path
     *
     * @return string|null
     */
    public function readFromManifest($path)
    {
        return MixService::get($path);
    }
}