<?php
/**
 * MixVersion plugin for Craft CMS
 *
 * Mix Service
 *
 * @author    Adrien Picard
 * @copyright Copyright (c) 2017 Adrien Picard
 * @package   Mix
 * @since     0.1.0
 */

namespace apstudio\mixversion\services;

use craft\base\Component;

class MixService extends Component{
  public static function get($path){
    $manifest = json_decode(file_get_contents('mix-manifest.json'), true);

    if(!isset($manifest[$path])){
      return null;
    }

    if(file_exists('./hot')){
      return file_get_contents('./hot').$manifest[$path];
    }else{
      return $manifest[$path];
    }
  }
}