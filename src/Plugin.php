<?php
namespace apstudio\mixversion;

use Craft;

use apstudio\mixversion\twig\MixTwigExtension;

class Plugin extends \craft\base\Plugin
{
    public function init()
    {
        parent::init();

        if (Craft::$app->request->getIsSiteRequest()) {
            Craft::$app->view->registerTwigExtension(new MixTwigExtension());
        }
    }
}